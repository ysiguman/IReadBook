<?php
require 'mvc/autoload.php';

// ##############
//  > Variables globales
// ##############
define ('DIR_ROOT', __DIR__);
define ('DIR_CLASS', __DIR__.'/controllers/');
define ('DIR_MODEL', __DIR__.'/models/');
define ('DIR_VIEW',  __DIR__.'/views/');
define ('DIR_PHOTOS',  __DIR__.'/photos/');


// ##############
//  > Inclusion des fichiers contenants les controlleurs et les modèles
// ##############
require DIR_MODEL . 'book.php';
require DIR_CLASS . 'books.php';

require DIR_MODEL . 'user.php';
require DIR_CLASS . 'users.php';

require DIR_MODEL . 'author.php';
require DIR_CLASS . 'authors.php';

require DIR_MODEL . 'tag.php';
require DIR_CLASS . 'tags.php';

require DIR_MODEL . 'list.php';
require DIR_CLASS . 'lists.php';

require DIR_MODEL . 'comment.php';
require DIR_CLASS . 'comments.php';

require DIR_MODEL . 'like.php';
require DIR_CLASS . 'likes.php';




// ##############
//  > Declaration des Routes ==> URLs accessibles
// ##############

// Initialisation du router
$router = new Router();

$router->addRoute(BASESERV . "/", "Books#getAll");
$router->addRoute(BASESERV . "/search", "Books#search");
$router->addRoute(BASESERV . "/books", "Books#getAll");
$router->addRoute(BASESERV . "/book/new", "Books#newBook");
$router->addRoute(BASESERV . "/books/create", "Books#create");

$router->addRoute(BASESERV . "/books/{i}", "Books#getById");
$router->addRoute(BASESERV . "/books/{i}/read", "Books#getById");
$router->addRoute(BASESERV . "/books/{i}/delete", "Books#delete");
$router->addRoute(BASESERV . "/books/{i}/updateBook", "Books#updateBook");
$router->addRoute(BASESERV . "/books/{i}/update", "Books#update");
$router->addRoute(BASESERV . "/books/{i}/lists", "Lists#getForBook");


$router->addRoute(BASESERV . "/authors", "Authors#getAll");
$router->addRoute(BASESERV . "/authors/{i}", "Authors#getBooks");
$router->addRoute(BASESERV . "/authors/{i}/delete", "Authors#delete");

$router->addRoute(BASESERV . "/create", "Users#createUser");
$router->addRoute(BASESERV . "/connexion", "Users#connexion");
$router->addRoute(BASESERV . "/disconnect", "Users#disconnect");
$router->addRoute(BASESERV . "/user/connexion", "Users#error_connect");

$router->addRoute(BASESERV . "/tags", "Tags#getAll");
$router->addRoute(BASESERV . "/tags/{s}", "Tags#getBooks");

$router->addRoute(BASESERV . "/lists", "Lists#getAll");
$router->addRoute(BASESERV . "/lists/{i}", "Lists#getBooks");
$router->addRoute(BASESERV . "/lists/{i}/add", "Lists#addBook");
$router->addRoute(BASESERV . "/lists/{i}/remove", "Lists#remove");
$router->addRoute(BASESERV . "/lists/{i}/delete", "Lists#delete");
$router->addRoute(BASESERV . "/lists/create", "Lists#create");
$router->addRoute(BASESERV . "/user/{i}/lists", "Lists#listBy");

$router->addRoute(BASESERV . "/com/{i}", "Coms#getAll");
$router->addRoute(BASESERV . "/com/create", "Coms#create");

$router->addRoute(BASESERV . "/books/{i}/like", "Likes#addLike");
$router->addRoute(BASESERV . "/books/{i}/dislike", "Likes#dislike");
$router->addRoute(BASESERV . "/user/likes", "Likes#myLikes");

// On lance la page appellée en donant l'url entrante en paramètre
$router->run($_SERVER['REQUEST_URI']);

?>