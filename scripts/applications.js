$( document ).ready(function() //   Attend que le document soit entièrement chargé
{
//  Se lance lors du clique sur l'elément #conn
    $(" #conn ").click(function () {
        var popUp = $(" #connPopUp ");
        popUp.removeClass("hidden");
    });

//  Se lance lors du clique sur l'elément #conn
    $(" #conn ").click(function () {
        var popUp = $(" #connexion_window ");
        popUp.removeClass("hidden");

        $(" #closeForm ").click(function () {
            popUp.addClass("hidden");           
        });
    });

//  Se lance lors du clique sur l'elément #connexion_window
//  Permet de desaficher la fenetre de connexion
    $('#connexion_window').click(function ( event ) {
        if( $(event.target).is($('#connexion_window'))) {
            $('#connexion_window').addClass('hidden');
        }
    });

//  Se lance lors du clique sur l'elément #selCreate
//  Permet de selectionner l'onglet selCreate
    $(" #selCreate ").click(function () {
        $(".connexion_logIn").removeClass('visible');
        $("#selLogIn").addClass('nselected');
        $("#selCreate").removeClass('nselected');
        $(".connexion_create").addClass('visible');
    });

//  Se lance lors du clique sur l'elément #selLogIn
//  Permet de selectionner l'onglet selLogIn
    $(" #selLogIn ").click(function () {
    	$(".connexion_create").removeClass('visible');
    	$("#selCreate").addClass('nselected');
    	$("#selLogIn").removeClass('nselected');
    	$(".connexion_logIn").addClass('visible');
    });

});

//  
function empt(id)
{
    console.log($("#" + id));
    $("#" + id).val("");
}

//  Affiche les listes dispo pour le livre
function showLists()
{
    if ($(".lists").hasClass("show")) {
        $(".lists").removeClass('show');
    } else {
        $(".lists").addClass('show');
    }
}

//  Permet de gerer les likes
var Likes = function (arg, id)
{
    this.nbLikes = arg.nbLikes;
    this.liked = arg.liked;
    this.id = id;

    this.like = function ()
    {
        if (this.liked == 1)
        {
            this.nbLikes--;
            this.liked = 0;
            $("#like_icon-" + this.id).html('<i class="fa fa-heart-o" aria-hidden="true"></i>');
            var xhr = new XMLHttpRequest();
            xhr.open('POST', BASESERV + '/books/' + this.id + '/dislike');
            xhr.send();
            new Notify("Like removed");
        } else
        {
            this.nbLikes++;
            this.liked = 1;
            $("#like_icon-" + this.id).html('<i class="fa fa-heart" aria-hidden="true"></i>');
            var xhr = new XMLHttpRequest();
            xhr.open('POST', BASESERV + '/books/' + this.id + '/like');
            xhr.send();
            new Notify('Like add');
        }
        $("#nbLikes-" + this.id).html(this.nbLikes);
    }
    this.show = function ()
    {
        console.log(this.nbLikes);
    }
}

//  Permet d'ajouter un livre à une liste
function addBook(list_id, book_id)
{
    console.log("List ID: " + list_id);
    console.log("Book ID: " + book_id);

    $.post( BASESERV + "/lists/" + list_id + "/add", { bookID: book_id}, function( result ) {
        if(result == 1)
        {
            $("#add_icon-" + list_id).html('<i class="material-icons">done</i>');
        }
    });
}

//  Permet la gestion des listes
var List = function (arg, book_id)
{
    this.list_id = arg.list_id;
    this.book_id = book_id;
    this.listed = arg.listed
    this.list_name = arg.name;

    this.list = function ()
    {
        if (this.listed == 1) {
            _self = this;
            $.post( BASESERV + "/lists/" + this.list_id + "/remove", { bookID: this.book_id}, function( result ) {
                if(result == 1)
                {
                    console.log("Rm : " + result);
                    $("#add_icon-" + _self.list_id).html('<i class="material-icons">add</i>');
                    new Notify("Removed to the list: " + _self.list_name);
                }
            });
            this.listed = 0;
        } else
        {
            _self = this;
            $.post( BASESERV + "/lists/" + this.list_id + "/add", { bookID: this.book_id}, function( result ) {
                if(result == 1)
                {
                    console.log("Add : " + result);
                    $("#add_icon-" + _self.list_id).html('<i class="material-icons">done</i>');
                    new Notify("Add to the list: " + _self.list_name);
                }
            });
            this.listed = 1;
        }
    }
}

//  Permet de gerrer les notification push
var Notify = function (message, state, timer) {
    this.zone = document.getElementById('notify');
    this.message = message;
    this.notification;
    this.state = 1;

    this.notification = this.zone.appendChild(document.createElement('div'));
    this.notification.innerHTML = message;
    switch (state)
    {
        case "success":
            this.notification.className += state;
            break;
        case "warning":
            this.notification.className += state;
            break;
        case "error": 
            this.notification.className += state;
            break;
    }

    if ( isNaN(timer) )
    {
        timer = 6000;
    }
    
    var self = this;
    setTimeout( function() { 
        if ( self.state == 1 )
        {
            self.destroy(); 
        }
    }, timer);

    this.destroy = function () {
        self.notification.className += " remove";
        var _this = self;
        setTimeout (function () {
            _this.zone.removeChild(_this.notification);
        }, 400);
    }
    this.print = function () {
        console.log(this.notification);
    }
    this.notification.onclick = function() {
        self.destroy();
        self.state = 0;
    }
}

//  Permet la gestions des listes dans les écrans de listes de livres
function lists (id)
{
    var popWindow = $("#lists_window");
    var list = $("#lists_window .list_frame ul");
    var loader = $("#lists_window .loader");
    var lists = new Array();


    popWindow.removeClass("hidden");
    
    $.get(BASESERV + "/books/" + id + "/lists", function( data ) {
        for (var i = data.length - 1; i >= 0; i--) {
            window.lists[i] = new List(data[i], id);
            list.append(listElementGenerate(data[i].name, data[i].list_id, data[i].listed, i));
            loader.addClass("hidden");
        }


        popWindow.click(function ( event ) {
            if( $(event.target).is(popWindow)) {
                popWindow.addClass('hidden');
                list.empty();
                loader.removeClass("hidden");
            }
        });
    });
}

//  Permet l'affichage de l'ecran de partage
function share (link, name, type)
{
    console.log("Link: " + link);
    console.log("Book: " + name);
    console.log("Type: " + type);

    var pathArray = location.href.split( '/' );
    var protocol = pathArray[0];
    var host = pathArray[2];
    var url = protocol + '//' + host;

    var share_window = $("#sharing_window");
    var facebook    = $(".facebook");
    var twitter     = $(".twitter");
    var plus        = $(".plus");
    var facebook_url    = "https://www.facebook.com/sharer/sharer.php?u=";
    var twitter_url     = "https://twitter.com/share?url=";
    var plus_url        = "https://plus.google.com/share?url=";

    share_window.addClass("sharing_window");

    share_window.click(function ( event ) {
        if( $(event.target).is(share_window)) {
            share_window.removeClass('sharing_window');
        }
    });

    var text = name;

    if (type == "book")
    {
        text = "I discover an amazing book : " + name;
    } else if (type == "list")
    {
        text = "Here some amazings books";
    } else if (type == "author")
    {
        text = "Here a great author";
    }

    facebook.click(function () {
        console.log(facebook_url + url + link + "&t=" + name);
        window.open(facebook_url + url + link + "&t=" + text, '',  'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=700');
    });
    twitter.click(function () {
        console.log("twitter");
        console.log(twitter_url + url + link + "&t=" + name);
        window.open(twitter_url + url + link + "&text=" + text, '',  'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=700');
    });
    plus.click(function () {
        console.log("plus");
        console.log(plus_url + url + link + "&t=" + name);
        window.open(plus_url + url + link + "&text=" + text, '',  'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=500,width=700');
    });
}

//  Permet de generer une liste de listes
function listElementGenerate(list_name, list_id, listed, index)
{
    var element = "<li onclick='window.lists[" + index + "].list()'>";
    element += "<span id='add_icon-" + list_id + "'>";
    element += listed == 0 ? "<i class='material-icons icons_edit'>add</i>" : "<i class='material-icons icons_edit'>done</i>";
    element += "</span>";
    element += list_name;
    element += "</li>";

    return element;
}


function addList()
{
    var list_window = $("#addList");

    list_window.removeClass("hidden");

    list_window.click(function ( event )
    {
        if($(event.target).is(list_window))
        {
            list_window.addClass("hidden");
        }
    });
}

//  Permet la validation du formulaire de nouveau livre
function validateFormNew()
{
    var title = $(".title");
    var author = $(".author");

    if (title.val() === "") {
        console.log("Mettre un titre");
        title.addClass("input_err");
        title.css("border", "2px solid #de5a7f");
        setTimeout(function() {
            title.removeClass("input_err");
        }, 500);
    }

    if (author.val() === "") {
        console.log("Mettre un auteur");
        author.addClass("input_err");
        author.css("border", "2px solid #de5a7f");
        setTimeout(function() {
            author.removeClass("input_err");
        }, 500);
    }
}

function validateFormConn(input)
{
    form = $(input.form);
    var name = form.find('#nameConn');
    var pass = form.find('#passConn');

    console.log(name);
    console.log(pass);

    if (name.val() === "") {
        console.log("Mettre un titre");
        name.addClass("input_err");
        name.css("border", "2px solid #de5a7f");
        setTimeout(function() {
            name.removeClass("input_err");
        }, 500);
        form.prepend("<div class='error'>Warning you must to give a userid !</div>");
    }

    if (pass.val() === "") {
        console.log("Mettre un auteur");
        pass.addClass("input_err");
        pass.css("border", "2px solid #de5a7f");
        setTimeout(function() {
            pass.removeClass("input_err");
        }, 500);
        form.prepend("<div class='error'>Warning you must to give a password !</div>");
    }
}
function wikipedia ()
{
    var title = $(".newForm .title").val();
    var spinner = $(".spinner");
    var wiki_url = "https://fr.wikipedia.org/w/api.php?action=opensearch&prop=extracts&format=json&callback=WRITE_DATA&search=" + title;
    // var wiki_url = "https://fr.wikipedia.org/w/api.php?action=query&prop=extracts&format=json&callback=WRITE_DATA&titles=" + title;

    if (title === "") {
        new Notify('Need a title');
    } else
    {   
        spinner.addClass("fa-spin");
        $.ajax({
                dataType: "jsonp",
                url: wiki_url,
                jsonp: 'jsonp',
                success: function(data) {
                  console.log(data);
                }
            });
    }
}
function WRITE_DATA (data)
{
    console.log(data);
    if (data[1].length === 0 && data[2].length === 0) {
        new Notify("Nothing match with the title");
    } else
    {
        var spinner = $(".spinner");
        $(".synopsis").val(data[2]);
        spinner.removeClass("fa-spin");
    }
}