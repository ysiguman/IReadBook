<?php

// #####################################
// ##  Ce fichier instancie les Objets 
// ##  Permettant les interactions MVC
// ##  Päradigme permettant de "simplifier"
// ##  la gestion du code et le rendre
// ##  plus propre ...
// #####################################


namespace MVC;
use PDO;

// Creation de la classe modele qui sera appellee a chaque nv modele
class Model 
{
    public function __construct() {

    }        

    // On creer une methode qui connecte la bdd
    protected static function setBdd ()
    {
        $bdd = new PDO("mysql:host=" . HOST . ";dbname=" . DB_NAME . ";charset=utf8", DB_USER, DB_PASS);
        return $bdd;
    }
}


// Creation de la classe vue qui permet de gerer et creer les vues
class View 
{
    //  Les variables necessaire à la vue
    private $view;
    private $data;
    private $title = "IReadBooks";
    public $page;
    public $user;

    public function __construct ($view, $data) 
    {
        $this->view = "_$view.php";
        $this->data = $data;

        session_start();
    }

//  Dernière inclusion et permet de renvoyer un fichier html complet
    public function render ()
    {
        include DIR_VIEW . "_layout.php";
    }

//  S'occupe de charger les différents fichiers de vues necessaire
    private function viewFiles () 
    {
        include DIR_VIEW . "_header.php";
        echo "<section>";
        if (isset($_SESSION["name"])) //    On inclus le panel seulement si l'utilisateur est connecté
        {
            include DIR_VIEW . "_userpannel.php";
        }
        echo "<div class='content'>";
        include DIR_VIEW . "_inner_nav.php";
        include DIR_VIEW . $this->view;
        echo "</div>";
        echo "</section>";
        include DIR_VIEW . "_connexion.php";
        include DIR_VIEW . "_notification.php";
        include DIR_VIEW . "_popLists.php";
        include DIR_VIEW . "_popShare.php";
        include DIR_VIEW . "_addList.php";
    }
}


class Controller 
{
    public function __construct () 
    {}
}
?>