<?php

// #####################################
// ##  Ce fichier implemente le routeur
// ##  couplé au mvc
// #####################################

class Router
{
	//	Tableau qui represente toutes les vues ajoutée à l'objet
	private static $routes = array();
	
	function __construct () {}

//	Fonction qui permet d'ajouter une nouvelle route au router
	public function addRoute ($url, $function)
	{
		//	On associe le parterne de l'url
		//	Le paterne correspond à une expression régulière avec laquelle on comparrera
		//	l'url en entrée pour savoir quel controller il faut appeler
		//	On decide de representer les variables nombre par : {i}
		//	Et les variables textuels par : {a} 
		//	Il nous faut alors remplacer leur occurence par la regex qui permet de les trouver:
		//	"[0-9]+" pour les nombres
		//	"(?i)[a-z](?-i).*" pour une chaine n'importe quel caractère insensible à la case
		$pattern = '/^' . str_replace(array('/', '{i}', '{s}'), array('\/', '[0-9]+', '(?i)[a-z](?-i).*'), $url) . '[\/]*$/';
		
		//	On prend comme index le paterne et on lui associe les variables necessaires
		self::$routes[$pattern]["url"] = $url;
		self::$routes[$pattern]["class"] = explode("#",$function)[0];
		self::$routes[$pattern]["method"] = explode("#",$function)[1];
	}

//	Fonctino qui permet d'analyser l'url entrante, determine le controller à appeller et le lance
	public function run ($url)
	{
		//	Si il y a des variables d'envoyé, on ne garde que l'url pure
		$url = explode("?", $url)[0];

		//	On boucle sur toutes les routes pour trouver laquelle correspond
		foreach (self::$routes as $pattern => $callback) {
			if (preg_match($pattern, $url)) //	Si une route correspond
			{
				if (preg_match('{i}', $callback['url'])) //	Si on detecte une variable de type {i}
				{
					//	On récupère le paramètre et appel le controller::methode avec le paramètre
					$params = explode("/", $url)[array_search('{i}',  explode('/', $callback["url"]))];
					return call_user_func(array($callback["class"], $callback["method"]), $params);
				}
				if (preg_match('{s}', $callback['url']))  //	Si on detecte une variable de type {i} 
				{
					//	On récupère le paramètre et appel le controller::methode avec le paramètre
					$params = explode("/", $url)[array_search('{s}',  explode('/', $callback["url"]))];
					return call_user_func(array($callback["class"], $callback["method"]), $params);
				}
				//	Aucun paramètre n'est attendu
				return call_user_func(array($callback["class"], $callback["method"]));
			}
		}
		//	Si aucune route correspond, on retourne une erreur
		echo "404: $url no match";
	}

//	Fonction qui permet de recperer l'url d'un controleur donné en entré sous la forme :
//	<Controleur>#<method>
	public function getRoute ($function, $params = "")
	{
		//	On sépare l'entrée par le caractère #
		$function = explode("#", $function);

		//	On boucle sur toutes les routes pour trouver laquelle correspond
		foreach (self::$routes as $pattern => $callback) {
			if ($callback["class"] === $function[0] && $callback["method"] === $function[1]) 
			{
				//	Une route correspond
				if (isset($params) && preg_match('{i}', $callback['url']))
				{
					//	On remplace le caractère {i} par le paramètre et on retourne l'url
					return str_replace("{i}", $params, $callback["url"]);
				} else if (isset($params) && preg_match('{s}', $callback['url'])) 
				{
					//	On remplace le caractère {s} par le paramètre et on retourne l'url
					return str_replace("{s}", $params, $callback["url"]);
				}
				return $callback["url"];
			}
		}
	}

}

?>