<!--
	#################################
	###		Vue qui permet d'afficher une fenetre avec 
	###		un formulaire d'ajout de liste
	#################################
-->

<div class="addLists_window hidden" id="addList">
	<form action="<?php echo $router->getRoute("Lists#create"); ?>" method="POST">
		<!-- Affiche "Name" comme label -->
		<input type="text" name="name" class="name_list" required>
		<label for="name">Name</label>

		<!--Bouton save-->
		<input type="submit" name="submit" value="Save">
	</form>
</div>
