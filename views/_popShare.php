<!--
	#################################
	###		Vue générée pour partager un livre
	#################################
-->

<div class="sharing_window_hidden" id="sharing_window">
	<div class="sharing_frame">
		<div class="title">Share with :</div>
 		<!-- Element permettant de partager avec Facebook -->
		<div class="facebook ntw">
			<div class="icon"><i class="fa fa-facebook" aria-hidden="true"></i></div>
			<span>Facebook</span>
			 <!-- Icons facebook provien de la librairie d'icone: FontAwesome-->
			<div class="send"><i class="material-icons">send</i></div>
		</div>
		
 		<!-- Element permettant de partager avec Twitter -->		
		<div class="twitter ntw">
			<div class="icon"><i class="fa fa-twitter" aria-hidden="true"></i></div>
			<span>Twitter</span>
			<!-- Icons Twitter provien de la librairie d'icone: FontAwesome-->
			<div class="send"><i class="material-icons">send</i></div>
		</div>
		<!-- Element permettant de partager avec Google + -->
		<div class="plus ntw">
			<div class="icon"><i class="fa fa-google-plus" aria-hidden="true"></i></div>
			<span>Google +</span>
			<!-- Icons Google provien de la librairie d'icone: FontAwesome+ -->
			<div class="send"><i class="material-icons">send</i></div>
		</div>

	</div>
</div>
