<!--
	#################################
	###		Vue générée pour ajouter un livre
	#################################
-->

<!--  Formulaire de création -->
<form method="post" enctype="multipart/form-data" action="<?php echo $this->data['post_url'] ?>" class="newForm">

	<!-- Affiche "Title..." à l'interieur de l'input à la place du texte à ecrire -->
	<input class="title" type="text" name="title" placeholder="Title ..." value="<?php echo$this->data['title']?>" <?php if ($this->page == "new"): echo "required"; endif;?>>


	<!-- Affiche "Author..." à l'interieur de l'input à la place du texte à ecrire -->
	<input id="author" class="author" type="text" name="author" placeholder="Author ..." value="<?php echo$this->data['author']?>" <?php if ($this->page == "new"): echo "required"; endif;?>>
	<div id="autocompleteAuthor" class="autocomplete"></div>

	<!-- Affiche "Category." à l'interieur de l'input à la place du texte à ecrire -->
	<input id="tag" class="category" type="text" name="category" placeholder="Category ..." value="<?php echo$this->data['category']?>">
	<div id="autocompleteTag" class="autocomplete"></div>

	<!-- Affiche "Synopsis..." à l'interieur de l'input à la place du texte à ecrire -->
	<textarea class="synopsis" name="synopsis" placeholder="Synopsis ..."><?php echo$this->data['synopsis']?></textarea>
    

	<!-- Permet d'ajouter une image au livre" -->
    <span>
        <input type="file" name="photo" id="photo"></th>
    </span>
	<span class="synch" onclick="wikipedia()">
		<i class="fa fa-wikipedia-w" aria-hidden="true"></i>
		Get Wikipedia data
		<i class="fa fa-refresh spinner"></i>
	</span>

	<!-- Affiche le bouton submit, un jeu des elements permets de styliser le bouton -->
	<div class="hCenter">
		<div class="button">
			<div class="back"></div>
			<input class="front" type="submit" name="submit" <?php if ($this->page == "new") :?> onclick="validateFormNew()" <?php endif;?>>
		</div>
	</div>
</form>

<script type="text/javascript">
    var tags = $.map(<?php echo json_encode(Tag::getAll()); ?>, function(value, index) {
	    return value.name;
	});
	var authors = $.map(<?php echo json_encode(Author::getAll()); ?>, function(value, index) {
	    return value.name;
	});
	console.log(authors);
    $( "#tag" ).autocomplete({
		source: tags,
		appendTo: "#autocompleteTag",
		classes: {
			"ui-state-focus": "highlight"
		}
	});
    $( "#author" ).autocomplete({
		source: authors,
		appendTo: "#autocompleteAuthor",
		classes: {
			"ui-state-focus": "highlight"
		}
	});
</script>