
<!--
	#################################
	###		Vue générée lors de l'ouverture d'une session 
	#################################
-->

<nav>
	<!-- Menu "Actions" -->
	<div class="nav_row">
		<h2 class="nav_title">Actions</h2>
		<div class="center">
			<!-- Liens "+Book" et "+List" -->
			<a class="button_actions actions" href="<?php echo $router->getRoute('Books#newBook'); ?>">+ Book</a>
			<button class="button_actions actions" onclick="addList()">+ List</button>
		</div>
	</div>
	<hr>
	<div class="nav_row">
		<!-- Menu "User + nom" de l'utilisateur-->
		<h2 class="nav_title">User <span class="user"><?php echo $_SESSION["name"]; ?></span>
				<!-- Fonction permettant de se déconnecter -->
				<a href="<?php echo  $router->getRoute('Users#disconnect'); ?>" class="discon"><i class="material-icons disconnect">highlight_off</i> disconnect</a></h2>
		<ul class="nav_list">
			<!-- Liens "My Books", "My Lists" et "My Likes" -->
			<li class="nav_list_el">My Books</li>
			<li class="nav_list_el <?php if($this->user == 'lists') {echo 'selected';} ?>"><a href="<?php echo $router->getRoute("Lists#listBy", $_SESSION['id']);?>">My Lists<a></li>
			<li class="nav_list_el <?php if($this->user == 'my_likes') {echo 'selected';} ?>"><a href="<?php echo $router->getRoute("Likes#myLikes");?>">My Likes</a></li>
		</ul>
	</div>
	<hr>
	<div class="nav_row">
		<!-- Menu Tags -->
		<h2 class="nav_title">Tags</h2>
		<ul class="nav_list">
<?php
	

	//Affiche toutes les categories des livres présent dans la liste
	$tags = Tag::getAll();
	foreach ($tags as $tag) {
?>
			<li class="nav_list_el tags">
				<a href="<?php echo $router->getRoute('Tags#getBooks',$tag->name); ?>"><?php echo $tag->name ?></a>
			</li>
<?php
	}
?>
		</ul>
	</div>
</nav>
