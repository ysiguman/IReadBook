<!--
	#################################
	###		Vue générée pour afficher un menu proposant un onglets des principales catégories
	#################################
-->
<?php 
	if(isset($this->page))
	{
?>
<div class="inner_nav <?php if(isset($this->page)) { echo $this->page; }?>">
	<ul>
		<li class="nav_book"><a href="<?php echo $router->getRoute('Books#getAll') ?>">books</a></li>
		<li class="nav_author"><a href="<?php echo $router->getRoute('Authors#getAll') ?>">authors</a></li>
		<li class="nav_list"><a href="<?php echo $router->getRoute('Lists#getAll') ?>">lists</a></li>
		<li class="cursor"></li>
	</ul>
	<div class="cursor_slider">
	</div>
</div>
<?php
	}
?>