<!--
	#################################
	###		Vue générée lors d'un mauvais identifiant 
	#################################
-->

<!-- Sensiblement similaire à la vu : "_connection.php" -->
<div class="connexion_window" id="connexion_window">
	<div class="connexion_frame">
<?php
	if(!isset($_SESSION['name']))
	{
?>
		<ul class="nav">
			<li id="selLogIn"><a href="#">Log In</a></li>
			<li class="nselected" id="selCreate"><a href="#">Create Account</a></li>
		</ul>

		<!-- Affiche "Connexion error bad id or bad password" puisque la vue est générer suite à une erreur de connection -->
		<div class="error">Connexion error bad id or bad password</div>
		<div class="connexion_logIn visible">

			<form class="connForm" action="<?php echo  $router->getRoute("Users#connexion"); ?>" method="POST">
				<div class="formRow">
					<input type="text" name="name" placeholder="User">
					<input type="password" name="pass" placeholder="password">
				</div>
				<div class="formRow">
					<input type="submit" name="submit" class="connexion_send">
				</div>
			</form>
		</div>
		<div class="connexion_create">
			<form class="connForm" action="<?php echo  $router->getRoute("Users#createUser"); ?>" method="POST">
				<div class="formRow">

					<input type="text" name="name" placeholder="User">
					<input type="password" name="pass" placeholder="password">
				</div>
				<div class="formRow">
					<input type="submit" name="submit" class="connexion_send">
				</div>
			</form>
		</div>
<?php
} else
{
?>
	<form class="disconnect" action="<?php echo  $router->getRoute("Users#disconnect"); ?>" method="POST">
			<input type="submit" name="submit" value="disconnect" class="connexion_disconnect">
		</div>
	</form>
<?php
}
?>
	</div>
</div>