<!--
	#################################
	###		Vue générée pour La liste de base des livres
	#################################
-->
<!-- Sensiblement similaire à la vue _list.php -->


<!-- Javascript qui permet de gerer l'ajout des likes -->
<script type="text/javascript">
	var likes = new Array();
</script>

<div class="books-section">

<!-- Si pas de livre alors le message "No books Finds" s'affiche -->
<?php
if(count($this->data) === 0)
{
?>
	<p>No Books Finds</p>
<?php
// S'il y a des livres, alors les livres d'affichent
} else
{
	$i = 0;
	foreach ($this->data as $book) {
		$likes = Like::getAll($book->book_id);
?>
		<script type="text/javascript">
    		likes[<?php echo $i; ?>] = new Likes (<?php echo json_encode($likes) . "," . $book->book_id; ?>);
		</script>

		<span class="book">
			<div class="book_img">
				<img src="<?php echo BASESERV ?>/photos/<?php echo $book->photo; ?>">
			<?php
				
				if ($book->category !== '')
				{ 
			?>
					<span class="tag"><?php echo $book->category; ?></span>
			<?php
				}
			?>
			</div>

			<div class="book_infos">
				<a href="<?php echo $router->getRoute("Books#getById", $book->book_id); ?>">
					<div class="book_name"><?php echo $book->title; ?></div>
					<div class="book_author"><?php echo $book->author; ?></div>
					<div class="back"></div>
					<div class="front"></div>
				</a>
				<div class="icons">
					
					<div>
					<i class="fa fa-share-alt" aria-hidden="true" onclick="share('<?php echo $router->getRoute("Books#getById", $book->book_id) . "','" . $book->title; ?>', 'book')"></i>
					</div>
					<?php if (isset($_SESSION["name"])) :?>
					<div>
						<i class="fa fa-plus" aria-hidden="true" onclick="lists(<?php echo $book->book_id; ?>)"></i>
					</div>
				<?php endif; ?>

					<div >
						<span class="text" id="nbLikes-<?php echo $book->book_id; ?>">
							<?php print_r($likes->nbLikes); ?>
						</span>
						<?php if (isset($_SESSION['name'])): ?>
							<span id="like_icon-<?php echo $book->book_id; ?>" onclick="likes[<?php echo $i; ?>].like()">
							<?php if($likes->liked) : ?>
								<i class="fa fa-heart" aria-hidden="true"></i>
							<?php else : ?>
								<i class="fa fa-heart-o" aria-hidden="true"></i>
							<?php endif; ?>
							</span>
						<?php else : ?>
							<i class="fa fa-heart-o" aria-hidden="true"></i>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</span>

<?php
	$i++;
	}
}
?>
</div>

