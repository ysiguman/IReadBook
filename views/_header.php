<!--
	#################################
	###		Vue générée pour en-tête
	#################################
-->

<?php
global $router;
?>
<header>

	<!-- Affiche Ireadbooks en haut à droite grace au CSS-->
	<a class="logo" href="<?php echo $router->getRoute('Books#getAll') ?>">IREADBOOKS</a>
	<div class="search material-icons">

		<!-- Champ qui permet de gerer le champ de recherche -->
		<form action="<?php echo $router->getRoute('Books#search'); ?>" method="GET">
			<input id="search" type="text" name="search" placeholder="Discover ..." required="required">
			<!-- Affiche l'icone "clear" lorque le champ input est focus  -->	
			<i onclick="empt('search')" class="material-icons clear">highlight_off</i>
			<!-- affiche une ligne qui s'affiche sous le input lorsqu'il est focus -->
			<div class="underline_search"></div>
		</form>
	</div>

	<!-- Affiche le nom de l'utilisateur si il est connectée, sinon juste l'icons du CSS -->
	<div class="connexion" id="conn">
		<span><i class="material-icons connIcon">account_circle</i> 
		<?php if (isset ($_SESSION['name'])): echo $_SESSION['name']; endif;?></span>
	</div>
</header>