<!--
	#################################
	###		Vue générée pour se connecter
	#################################
-->

<!-- Affiche la vue de la connexion ou la création d'un compte -->
<div class="connexion_window hidden" id="connexion_window">
	<div class="connexion_frame">
<!-- En fonction de si un utilisateur est connecté ou non on affiche les formulaires de connection ou de deconnection -->
<?php
	if(!isset($_SESSION['name']))
	{
?>
		
		<!-- Affiche les onglets Log In et Create Account -->
		<ul class="nav">
			<li id="selLogIn"><a href="#">Log In</a></li>
			<li class="nselected" id="selCreate"><a href="#">Create Account</a></li>
		</ul>

		<!-- Vue de la partie LogIn -->
		<div class="connexion_logIn visible">
			<form class="connForm" action="<?php echo  $router->getRoute("Users#connexion"); ?>" method="POST">
				<!-- "formRow" permet de gerer une ligne sur le formulaire -->
				<div class="formRow">
					<!-- Affiche les inputs "User" et "password"-->
					<input id="nameConn" type="text" name="name" placeholder="User" required>
					<input id="passConn" type="password" name="pass" placeholder="password" required>
				</div>

				<!-- Affiche le bouton d'envoie	-->
				<div class="formRow">
					<input type="submit" name="submit" class="connexion_send" onclick="validateFormConn(this)">
				</div>
			</form>
		</div>

		<!-- Vue pour la création d'un compte -->
		<div class="connexion_create">
			<form class="connForm" action="<?php echo  $router->getRoute("Users#createUser"); ?>" method="POST">
				<div class="formRow">
					<!-- Affiche les champs de texts "User" et "password"-->
					<input id="nameConn" type="text" name="name" placeholder="User" required>
					<input id="passConn" type="password" name="pass" placeholder="password" required>
				</div>
				<div class="formRow">
					<input type="submit" name="submit" class="connexion_send" onclick="validateFormConn(this)">
				</div>
			</form>
		</div>
<?php
} else
{
?>
	<!-- Vue pour se déconnecter de son compte	-->
	<form class="disconnect" action="<?php echo  $router->getRoute("Users#disconnect"); ?>" method="POST">
			<input type="submit" name="submit" value="disconnect" class="connexion_disconnect">
		</div>
	</form>
<?php
}
?>
	</div>
</div>