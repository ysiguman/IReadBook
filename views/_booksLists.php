<!--
	#################################
	###		Vue générée pour lister les livres présents dans
	###		une liste ou les livres d'un autheurs
	#################################
-->


<!-- 
	Javascript qui permet de gerer l'ajout des likes 
	Prepare un tableau qui se remplira avec les objets
	permettant la gestion des likes
-->
<script type="text/javascript">
	var likes = new Array();
</script>

<div class="list_header">
		<!-- 
			Affiche le nom de la liste
			En fonction du type de la liste affiche l'icone correspondante
		-->
	<h3>
		<?php if (isset($this->data->list_user)): ?>
			<i class="fa fa-list" aria-hidden="true"></i>
		<?php elseif (!isset($this->data->author_id) && !isset($this->data->list_user)) :?>
			<i class="fa fa-tags" aria-hidden="true"></i>
		<?php endif ?>
		<?php echo $this->data->name; ?>		
	</h3>

	<ul>
		<!-- Si la liste correspond à une liste, affiche le nom de l'utilisateur qui l'a crée -->
	<?php if (isset($this->data->list_user)): ?>
			<li>User: <?php echo $this->data->list_user ?></li>
	<?php endif ?>
		<!-- Affiche le nombre de livres créées dans la liste par l'utilisateur -->
		<li>Books: <?php echo $this->data->nbBooks ?></li>
	</ul>

	<!-- Permet d'afficher l'icone de suppression de la liste / de l'auteur -->
	<?php if (isset($this->data->list_user) && isset($_SESSION["name"])): ?>
			<a class="delete" href="<?php echo $router->getRoute("Lists#delete", $this->data->list_id)?>">
				<!-- Icone de suppression d'un livre -->
				<i class="material-icons icons_edit">delete</i> 
			</a>
	<?php elseif (isset($this->data->author_id) && isset($_SESSION["name"])) :?>
			<a class="delete" href="<?php echo $router->getRoute("Authors#delete", $this->data->author_id)?>">
				<!-- Icons de suppression d'un livre -->
				<i class="material-icons icons_edit">delete</i>
			</a>
	<?php endif ?>

	<!-- Affiche l'icone de partage en fonction du type recupère le bon lien -->
	<?php if (isset($this->data->list_user)): ?>
		<i class="material-icons icons_edit" aria-hidden="true" onclick="share('<?php echo $router->getRoute("Lists#getBooks", $this->data->list_id) . "','" . $this->data->name; ?>', 'list')">share</i>
	<?php elseif (isset($this->data->author_id)):?>
		<i class="material-icons icons_edit" aria-hidden="true" onclick="share('<?php echo $router->getRoute("Authors#getBooks", $this->data->author_id) . "','" . $this->data->name; ?>', 'author')">share</i>
	<?php endif ?>
		
</div>

<div class="books-section"> 	<!--Section qui vas afficher la liste de livre -->
<?php
$i = 0;
//Si il n'y a pas de livre dans un liste alors le message "Nothing found" s'affiche
if (count($this->data->books) === 0)
{
	echo "<p>Nothing Found.</p>";
} else {
	// Boucle pour afficher tous les livres de la liste
	foreach ($this->data->books as $book) {
		$likes = Like::getAll($book->book_id);
?>
		<!-- On remplis le tableau déclarer au début aec un nouvel objet de type Likes -->
		<script type="text/javascript">
    		likes[<?php echo $i; ?>] = new Likes (<?php echo json_encode($likes) . "," . $book->book_id; ?>);
		</script>

		<!-- Element Book -->  
		<span class="book">
			<div class="book_img">
				<img src="<?php echo BASESERV ?>/photos/<?php echo $book->photo; ?>">
			<?php
				if ($book->category !== '')
				{ 
			?>
				<!-- Affiche la catégorie de livre s'il en a une -->
				<span class="tag"><?php echo $book->category; ?></span>
 			<?php
				}
			?>
			</div>

			<!-- Affiche les informations du livre-->
			<div class="book_infos">
				<!-- Affiche l'auteur / le nom du livre, front et back sont juste là pour l'UI -->
				<a href="<?php echo $router->getRoute("Books#getById", $book->book_id); ?>">
					<div class="book_name"><?php echo $book->title; ?></div>
					<div class="book_author"><?php echo $book->author; ?></div>
					<div class="back"></div>
					<div class="front"></div>
				</a>

				<!-- Affiche quoi les icons share addToList et like en fonction de si un utilisateur est connecté -->
				<div class="icons">
					<div>
					<i class="fa fa-share-alt" aria-hidden="true" onclick="share('<?php echo $router->getRoute("Books#getById", $book->book_id) . "','" . $book->title; ?>', 'book')"></i>
					</div>

					<!-- Affiche l'icone si un utilisateur est connecté -->
					<?php if (isset($_SESSION["name"])) :?>
						<div>
							<i class="fa fa-plus" aria-hidden="true" onclick="lists(<?php echo $book->book_id; ?>)"></i>
						</div>
					<?php endif; ?>
					<div >
						<!-- Affiche le nombre de likes -->
						<span class="text" id="nbLikes-<?php echo $book->book_id; ?>">
							<?php print_r($likes->nbLikes); ?>
						</span>

						<!-- Affiche l'icone soit remplis soit vide en fonction de si le livre à été liké et de si un utilisateur est connecté -->
						<?php if (isset($_SESSION['name'])): ?>
							<span id="like_icon-<?php echo $book->book_id; ?>" onclick="likes[<?php echo $i; ?>].like()">
								<?php if($likes->liked) : ?>
									<i class="fa fa-heart" aria-hidden="true"></i>
								<?php else : ?>
									<i class="fa fa-heart-o" aria-hidden="true"></i>
								<?php endif; ?>
							</span>
						<?php else : ?>
							<i class="fa fa-heart-o" aria-hidden="true"></i>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</span>
<?php
	$i++;
	}
}
?>
</div>
