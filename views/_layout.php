<!--
	#################################
	###		Genere la structure de la page,
	###		récupère les différentes sources: css / fonts / js / ...
	#################################
-->
<!DOCTYPE html>
<html>
<head>
	<title><?php echo $this->title ?></title>
<!-- Styles css -->
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/css.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/newBook.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/header.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/book.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/listAuthors.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/inner_nav.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/listBooks.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/booksLists.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/connexion.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/userpannel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/addList.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/notification.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/popLists.css">
	<link rel="stylesheet" type="text/css" href="<?php echo BASESERV ?>/css/popShare.css">

<!-- Librairies -->
    <script type="text/javascript" src="https://unpkg.com/grade-js/docs/dist/grade.js"></script>
    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script> -->
    <script type="text/javascript" src="<?php echo BASESERV ?>/scripts/jquery.js"></script>
    <script type="text/javascript" src="<?php echo BASESERV ?>/scripts/jquery-ui.min.js"></script>

<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">

<!-- Scripts -->
	<script type="text/javascript" src="<?php echo BASESERV ?>/scripts/applications.js"></script>
</head>
<body>
	<script type="text/javascript">
		const BASESERV = "<?php echo BASESERV; ?>";
	</script>

	<?php $this->viewFiles() ?>

</body>
</html>