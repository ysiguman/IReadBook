<!--
	#################################
	###		Vue générée pour lister les différents auteurs
	#################################
-->

<div class="list_section">
<?php
// si pas de livre dans la liste d'un auteur alors affiche "Nothing found"
if(count($this->data) === 0)
{
	echo "<p>Nothing Found.</p>";
} else
{
	foreach ($this->data as $author) 
	{
	?>
	<div class="item <?php if(isset($_SESSION["name"])) { echo "connected"; }?>">
		<a class="item_link" href="<?php echo $router->getRoute("Authors#getBooks", $author->author_id); ?>">
			<span class="name"><?php echo $author->name; ?></span>
			<span class="books">Books: <strong><?php echo $author->nbBooks?></strong></span>
		</a>
		<?php
			if(isset($_SESSION["name"]))
			{ 
		?>
		<!-- Permet de supprimer un livre d'un auteur -->
			<a class="delete" href="<?php echo $router->getRoute("Authors#delete", $author->author_id)?>"><i class="material-icons icons_edit">delete</i></a>
		<?php
			}
		?>
	</div>
	<?php
	}
}
?>
</div>