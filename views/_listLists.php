<!--
	#################################
	###		Vue générée pour lister les differentes listes
	#################################
-->

<div class="list_section">
<?php

// Si pas de liste alors affiche "Nothing found"
if (count($this->data) === 0)
{
	echo "<p>Nothing Found.</p>";
} else {
	foreach ($this->data as $list) 
	{
?>

<!-- Si l'utisateur est connecté -->
<div class="item <?php if(isset($_SESSION["name"])) { echo "connected"; }?>">
	<a class="item_link" href="<?php echo $router->getRoute("Lists#getBooks", $list->list_id); ?>">
		<span class="name"><?php echo $list->name; ?></span>
		<span class="books">Books: <strong><?php echo $list->nbBooks?></strong></span>
	</a>
	<?php
		if(isset($_SESSION["name"]))
		{ 
	?>
		<!-- Permet de supprimer une liste -->
		<a class="delete" href="<?php echo $router->getRoute("Lists#delete", $list->list_id)?>"><i class="material-icons icons_edit">delete</i></a>
	<?php
		}
	?>
</div>
<?php
	}
}
?>
</div>