<!--
	#################################
	###		Vue générée pour un Livre
	#################################
-->

<!-- Javascript qui permet de gerer l'ajout des lists au livre -->
<script type="text/javascript">
	var list = new Array();	
</script>

<div class="bookHeader gradient-wrap">
	<div class="headerContent">

	<!-- Appel de l'image de la couverture -->
		<img src="<?php echo BASESERV ?>/photos/<?php echo $this->data->photo; ?>">
	
	<!-- Bloc contenant les listes pour lequel on peut rajouter notre livre -->
		<div class="lists">
			<ul>

<!-- PHP qui génère une boucle avec les données récuperer dans le modele ListBook -->
<?php
$lists = ListBook::getForBook($this->data->book_id);
$i = 0;
foreach ($lists as $list) {
?>
			<!-- Javascript qui creer un nouvel objet List pour chaque listes -->
			<script type="text/javascript">
				list[<?php echo $i; ?>] = new List(<?php echo json_encode($list) . ", " . $this->data->book_id; ?>);
			</script>

			<!-- Le onclick permet d'appeler un fonction Javacript pour la liste -->
			<li onclick="list[<?php echo $i; ?>].list()">
				<span id="add_icon-<?php echo $list->list_id; ?>">
				<!-- Le span affiche l'icone à afficher selon que le livre appartienne à la boucle -->
				<?php
					if ($list->listed) {
				?>
					<i class="material-icons icons_edit">done</i>
				<?php
					} else
					{
				?>
					<i class="material-icons icons_edit">add</i>
				<?php
				}
				?>
				</span> 

				<!-- Affiche le nom de la liste -->
				<?php echo $list->name; ?>
				</li>
<?php
$i++;
}
?>
				</ul>
		</div>

		<!-- Conteneur qui affiche les infos du livre -->
		<div class="headerInfos">

			<!-- Titre du Livre -->
			<h1 class="bookTitle"><?php echo $this->data->title; ?> 

		<!-- 	Determine si un utilisateur est connecté,
			Si Oui, lui donne les droits suivants: -->		
			<?php
				if (isset($_SESSION['name'])) {
			?>
				<a href="<?php echo $router->getRoute("Books#delete", $this->data->book_id); ?>">
					<i class="material-icons icons_edit">delete</i></a>
				<a href="<?php echo $router->getRoute("Books#updateBook", $this->data->book_id); ?>">
					<i class="material-icons icons_edit">mode_edit</i></a> 
					<i class="material-icons icons_edit" onclick="showLists()">playlist_add</i> 
			<?php
				}
			?>
			<i class="material-icons icons_edit" aria-hidden="true" onclick="share('<?php echo $router->getRoute("Books#getById", $this->data->book_id) . "','" . $this->data->title; ?>', 'book')">share</i>
				</h1>

		<!-- Affiche l'auteur et un lien vers ses livres  -->
			<a href="<?php echo $router->getRoute("Authors#getBooks", $this->data->author_id); ?>" class="bookAuthor"><?php echo $this->data->author; ?></a>

		<!-- Si le livre appartiens à une catégorie, affiche sa catégorie -->
			<?php
				if ($this->data->category != "") {
			?>
			<a class="bookCategory" href="<?php echo  $router->getRoute("Tags#getByName", $this->data->category); ?>"><?php echo $this->data->category; ?></a>
			<?php
				}
			?>

		<!-- Affiche les likes du livre, selon si le user peut l'etat du like,
		creer un nouvel objet JS et prepare l'icone correspondante -->
			<div class="likes" <?php if(isset($_SESSION["name"])) { ?>onclick="like.like()" <?php } ?>>
				<span id="like_icon-<?php echo $this->data->book_id; ?>">
				<?php
					$likes = Like::getAll($this->data->book_id);
					if ($likes->liked) {
				?>
					<i class="fa fa-heart" aria-hidden="true"></i>
				<?php
					} else
					{
				?>
					<i class="fa fa-heart-o" aria-hidden="true"></i>
				<?php
					}
				?>
				</span>
				<span id="nbLikes-<?php echo $this->data->book_id; ?>">
				<?php
					echo($likes->nbLikes);
				?>
				</span>
			</div>
		</div>
	</div>
</div>

<!-- affiche le synopsis -->
<div class="book_synopsis">
	<?php echo $this->data->synopsis; ?>
</div>


<!-- Zone de commentaire -->
<div class="comment_frame">
	<h3>Commentaires</h3>
	<div class="comments">

<?php
if ($this->data->coms != null)
{
// Si il y a des commentaires, les affichent 
	foreach ($this->data->coms as $com) {
?>
	<div class="comment">
		<span class="author"><?php echo $com->com_author; ?></span>
		<p><?php echo $com->com ?></p>
	</div>
<?php
	}
}
else
{
?>
<!-- S'il n'y en a pas, affiche: -->
	<h4>No Coms.</h4>
<?php
}
?>
	</div>

<!-- Si un utilisateur est connecté, lui permet d'ecrire un commentaire -->
<?php
	if(isset($_SESSION['name']))
	{
?>
	<hr />
	<div class="form_comment">
		<form action="<?php echo $router->getRoute("Coms#create"); ?>" method="POST">
			<textarea id="com" name="com" required="required"></textarea>
			<label for="com" >Write a com ...</label>
			<input type="hidden" name="user_id" value="<?php echo $_SESSION['id']; ?>">
			<input type="hidden" name="book_id" value="<?php echo $this->data->book_id; ?>">
			<input class="send" type="submit" name="submit">
			<button type="button" onclick="empt('com')">Empty</button>
		</form>
	</div>
</div>
<?php
	}
?>

<script type="text/javascript">
// Quand la page est chargée completement, ajoute un dégradé au conteneur d'Information
    window.addEventListener('load', function(){
        Grade(document.querySelectorAll('.gradient-wrap'))
    })

// SI l'utilisateur est connecté, creer un nouvel objet Like avec le nombre de 
// like du livre et l'état du like par rapport à l'utilisateur connecté
<?php
    if (isset($_SESSION["name"]))
    {
?>
    var like = new Likes (<?php echo json_encode($likes) . "," . $this->data->book_id; ?>);
<?php
	}
?>
</script>