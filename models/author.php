<?php
/*
#################################
###
###		AUTHOR MODELE
###
#################################
*/


//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;


//	Classe Author qui permet les interactions avec la base de donnée
//	elle hérite de la classe Model
//	Elle permet de récupérer les données concernant les authors dans la BDD
class Author extends mvc\Model
{
	//	Différentes variables de l'objet
	public $author_id;
	public $name;
	public $nbBooks;

//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle associe les paramètes aux variables de la classe
	public function __construct($id = "", $name = "", $nbBooks = 0)
	{
		$this->author_id = $id;
		$this->name = $name;
		$this->nbBooks = $nbBooks;
	}

//	Fonction qui permet de récuperer tous les autheurs présent en base de donnée
	public static function getAll ()
	{
		//	On initialise la base de donnée
		$bdd = self::setBdd ();

		//	On prepare la requete
		$sql = "SELECT * FROM iReadBook_author";

		//	On lance la requette
		$requete = $bdd->query($sql)->fetchAll();
		$authors = [];

		//	Boucle pour chaque élément de la requete, on crée un nouvel objet que l'on stock dans un tableau
		foreach ($requete as $author) {
			//	Requete pour récupérer l'auteur
			$sql = "SELECT COUNT(*) AS nbBook FROM iReadBook_book WHERE fk_author_id = " . $author["id"];
			
			//	Création de l'objet
			$authors[] = new Author(
				$author["id"],
				$author['name'],
				$bdd->query($sql, PDO::FETCH_OBJ)->fetch()->nbBook);
		}
		return $authors;
	}

//	Fonction qui recupère un auteur via son id
	public static function getById ($id)
	{
		//	On initialise la base de donnée
		$bdd = self::setBdd ();

		//	On prepare la requete
		$sql = "SELECT * FROM iReadBook_author WHERE id = $id";
		$author = $bdd->query($sql, PDO::FETCH_OBJ)->fetch();	//	On execute la requete

		//	On prepare une requete pour connaitre le nombre de livre
		$sql = "SELECT COUNT(*) AS nbBook FROM iReadBook_book WHERE fk_author_id = $id";

		//	On creer l'objet auteur
		$author = new Author(
			$id,
			$author->name,
			$bdd->query($sql, PDO::FETCH_OBJ)->fetch()->nbBook);

		return $author;
	}

//	Fonction qui supprime l'auteur de la base de donnée via son id
	public static function delete ($id)
	{
		$bdd = self::setBdd ();
		$sql = "DELETE FROM iReadBook_author WHERE id = $id";

		$bdd->exec($sql);
	}
}

?>