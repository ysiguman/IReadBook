<?php

/*
#################################
###
###		LIKE MODELE
###
#################################
*/

//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;

//	Classe Like qui permet les interactions avec la base de donnée
//	elle hérite de la classe Modele
//	Elle permet de récupérer les données concernant les listes dans la BDD
class ListBook extends mvc\Model
{
	//	Différentes variables de l'objet
	public $name;
	public $list_user;
	public $list_id;
	public $books;
	public $nbBooks;
	public $listed;

	//	Fonction constructor appellée lors de l'initialisation de la classe,
	//	Elle associe les paramètes aux variables de la classes
	public function __construct($name, $list_user, $list_id, $books, $nbBooks = 0, $listed = 0)
	{

		//	Différentes variables de l'objet
		$this->name = $name;
		$this->list_user = $list_user;
		$this->list_id = $list_id;
		$this->books = $books;
		$this->nbBooks = $nbBooks;
		$this->listed = $listed;
	}


	//	Fonction qui permet de récuperer toutes les listes présentes en base de donnée
	public static function getAll ()
	{
		//	On initialise la base de donnée
		$bdd = self::setBdd ();

		//	On prepare la requete
		$sql = "SELECT * FROM iReadBook_list";

		//	On lance la requete
		$requete = $bdd->query($sql)->fetchAll();
		$listBooks = [];

		//	Lis tous les éléments récupéré par la requete et crée une nouvelle liste
		foreach ($requete as $list) {
			$list_name = $list['list_name'];
			$user_id = $list["fk_list_user"];
			$list_id = $list["list_id"];

			//	Prépare le sql
			$sql = "SELECT name FROM iReadBook_user WHERE id = $user_id";

			//	Une seule valeur est censée etre retournée donc Fetch au lieu de FetchALl
			$requete = $bdd->query($sql)->fetch();

			$user_name = $requete['name'];

			//	Prépare le sql 
			$sql = "SELECT * FROM iReadBook_listBook WHERE fk_list_id = $list_id";

			//	Une seule valeur est censée etre retournée donc Fetch au lieu de FetchALl
			$requete = $bdd->query($sql)->fetchAll();

			$books = [];

			//	On boucle le résultat renvoyé et on créer pour chaque élément un objet Book
			foreach ($requete as $book) {
				$books[] = Book::getById($book['fk_book_id']);
			}

			//	On creer le sql pour récuperer seulent le nombre de livres présents sur la liste
			$sql = "SELECT COUNT(*) AS nbBook FROM iReadBook_listBook WHERE fk_list_id = " . $list_id;
			$listBooks[] = new ListBook($list_name, $user_name, $list_id, $books, $bdd->query($sql, PDO::FETCH_OBJ)->fetch()->nbBook); 
		}		
		return $listBooks;
	}

//	Fonction pour récupérer tous les livres en BDD dont la liste correspond à celui donné en paramètre
	public static function getForBook ($book_id)
	{
		$bdd = self::setBdd ();
		$sql = "SELECT * FROM iReadBook_list";
		$requete = $bdd->query($sql)->fetchAll();

		foreach ($requete as $list) {
			$list_name = $list['list_name'];
			$user_id = $list["fk_list_user"];
			$list_id = $list["list_id"];

			//	Prépare le sql
			$sql = "SELECT name FROM iReadBook_user WHERE id = $user_id";

			//	On lance la reqête
			//	Une seule valeur est censée etre retournée donc Fetch au lieu de FetchALl		
			$requete = $bdd->query($sql)->fetch();
			$user_name = $requete['name'];

			//	Prépare le sql
			$sql = "SELECT * FROM iReadBook_listBook WHERE fk_list_id = $list_id";

			//	Une seule valeur est censée etre retournée donc Fetch au lieu de FetchALl		
			$requete = $bdd->query($sql)->fetchAll();


			//	Lis tous les éléments récupéré par la requete et crée une nouvelle liste
			$listBooks = [];
			foreach ($requete as $book) {
				$books[] = Book::getById($book['fk_book_id']);
			}

			//	On prepare le sql et lance une requete pour recupérer le nombre de livres
			$sql = "SELECT COUNT(*) AS nbBook FROM iReadBook_listBook WHERE fk_list_id = " . $list_id;
			$listBooks[] = new ListBook($list_name, $user_name, $list_id, $books, $bdd->query($sql, PDO::FETCH_OBJ)->fetch()->nbBook, self::hasbook($list_id, $book_id)); 
		}
		return $listBooks;
	}
	
//	Determine si la liste à le livre donné en paramètre
	public static function hasbook($list_id, $book_id)
	{
		$bdd = self::setBdd ();
	    $sql = "SELECT COUNT(*) AS nb FROM iReadBook_listBook WHERE fk_book_id = $book_id AND fk_list_id = $list_id";

		$requete = $bdd->query($sql)->fetch();
		
		return $requete["nb"];
	}

//	Fonction qui permet de selectionner une liste avec son ID
	public static function getById ($id)
	{
		$bdd = self::setBdd ();
		$sql = "SELECT * FROM iReadBook_list WHERE list_id = $id";

		//	Une seule valeur est censée etre retournée donc Fetch au lieu de FetchALl				
		$requete = $bdd->query($sql)->fetch();

		$list_name = $requete['list_name'];
		$user_id = $requete["fk_list_user"];
		$list_id = $requete["list_id"];

		$sql = "SELECT name FROM iReadBook_user WHERE id = $user_id";

		//	Une seule valeur est censée etre retournée donc Fetch au lieu de FetchALl				
		$requete = $bdd->query($sql)->fetch();

		$user_name = $requete['name'];

		$sql = "SELECT * FROM iReadBook_listBook WHERE fk_list_id = $list_id";
		
		//	Une seule valeur est censée etre retournée donc Fetch au lieu de FetchALl		
		$requete = $bdd->query($sql)->fetchAll();

		$listBooks = [];
		foreach ($requete as $book) {
			$books[] = Book::getById($book['fk_book_id']);
		}
		$sql = "SELECT COUNT(*) AS nbBook FROM iReadBook_listBook WHERE fk_list_id = " . $list_id;
		$listBooks = new ListBook($list_name, $user_name, $list_id, $books, $bdd->query($sql, PDO::FETCH_OBJ)->fetch()->nbBook); 
		
		return $listBooks;
	}

//	Fonction qui permet de supprimer une liste
	public static function delete ($id)
	{
		$bdd = self::setBdd ();
		$sql = "DELETE FROM iReadBook_list WHERE list_id = $id";

		$bdd->exec($sql);  //exécute la fonction
	}

//	Récupère les listes d'un utilisateur
	public static function listBy ($user_id)
	{
		$bdd = self::setBdd ();
		$sql = "SELECT * FROM iReadBook_list WHERE fk_list_user = $user_id";

		$res = $bdd->query($sql)->fetchAll();

		$listBooks = [];
		foreach ($res as $list) {
			$list_name = $list['list_name'];
			$user_id = $list["fk_list_user"];
			$list_id = $list["list_id"];


			$sql = "SELECT name FROM iReadBook_user WHERE id = $user_id";
			$requete = $bdd->query($sql)->fetch();

			$user_name = $requete['name'];

			$sql = "SELECT * FROM iReadBook_listBook WHERE fk_list_id = $list_id";
			$requete = $bdd->query($sql)->fetchAll();


			foreach ($requete as $book) {
				$books[] = Book::getById($book['fk_book_id']);
			}
			
			$sql = "SELECT COUNT(*) AS nbBook FROM iReadBook_listBook WHERE fk_list_id = " . $list_id;
			$listBooks[] = new ListBook($list_name, $user_name, $list_id, $books, $bdd->query($sql, PDO::FETCH_OBJ)->fetch()->nbBook); 
		} 

		
		return $listBooks;
	}

//	Fonction qui permet d'ajouter un livre dans la liste
	public static function addBook($list_id, $book_id)
	{		
		$bdd = self::setBdd ();

		try
        {        	    
    	    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	    
    	    $sql = "INSERT INTO iReadBook_listBook (fk_book_id, fk_list_id)
    	    VALUES ('" . $book_id . "', '".
            	    $list_id."')";

            $bdd->exec($sql);  //exécute la fonction
            return 1;
    	}
    	catch(PDOException $e)
    	{
    	    echo $sql . "<br>" . $e->getMessage();
    	    return 0;
    	}
	}

//	Supprime un livre d'une liste
	public static function remove($list_id, $book_id)
	{
		$bdd = self::setBdd ();

		try 
        {        	    
    	    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	    
    	    $sql = "DELETE FROM iReadBook_listBook WHERE fk_book_id = $book_id AND fk_list_id = $list_id ";

            $bdd->exec($sql); //exécute la fonction et supprime book id
            return 1;
    	}
    	catch(PDOException $e)
    	{
    	    echo $sql . "<br>" . $e->getMessage();
    	    return 0;
    	}
	}
	
//	Creer une nouvelle liste, récupère le nom de la liste à créer et l'id de l'utilisateur créateur
	public static function create($list_name, $list_user_id)
	{
		$bdd = self::setBdd();
		$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	 
	    $sql = "INSERT INTO iReadBook_list (list_name, fk_list_user)
	            VALUES ('" . $list_name . "', '" . $list_user_id . "')";
	    $bdd->exec($sql);
	}
}

?>