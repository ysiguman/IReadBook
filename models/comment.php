<?php
/*
#################################
###
###		COMMENT MODELE
###
#################################
*/


//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;


//	Classe Com qui permet les interactions avec la base de donnée
//	elle hérite de la classe Modele
//	Elle permet de récupérer les données concernant les commentaires dans la BDD
class Com extends mvc\Model
{
	//	Différentes variables de l'objet
	public $com_author;
	public $com;
	public $com_date;
	public $com_id;
	public $com_book_id;

//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle associe les paramètes aux variables de la classe
	public function __construct($author, $com, $date, $id, $book_id)
	{	
		//	Différentes variables de l'objet
		$this->com_author = $author;
		$this->com = $com;
		$this->com_date = $date;
		$this->com_id = $id;
		$this->com_book_id = $book_id;
	}

//	Fonction qui permet de récuperer tous les commentaires présent en base de donnée
	public static function getAll ($book_id)
	{
		//	On initialise la base de donnée
		$bdd = self::setBdd ();

		//	On prepare la requete
		$sql = "SELECT * FROM iReadBook_comment WHERE fk_book_id = $book_id";

		//	On lance la requette
		$requete = $bdd->query($sql)->fetchAll();

		//	On boucle pour chaque élément de la requete 
		$coms =[];
		foreach ($requete as $com) {
			$coms [] = new Com(	User::getName($com['fk_user_id']),
								$com["comment"],
								$com["date"],
								$com["id"],
								$com["fk_book_id"]);
		}
		
		return $coms;
	}

//	Fonction qui permet d'ajouter un nouveau commentaire, prend en commentaire les données à utiliser
	public static function create ($data)
	{
		//	On initialise la base de donnée
		$bdd = self::setBDD();
        try 
        {        	    
    	    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	    
    	    //	Requete pour insérer un nouveau commentaire
    	    $sql = "INSERT INTO iReadBook_comment (fk_book_id, fk_user_id, comment)
    	    VALUES ('" . $data['book_id'] . "', '".
            	    $data['user_id']."', '".
            	    $data['com']."')";

            $bdd->exec($sql);

    	}
    	catch(PDOException $e)
    	{
    	    echo $sql . "<br>" . $e->getMessage();
    	}
	}
}

?>