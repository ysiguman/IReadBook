<?php
/*
#################################
###
###		BOOK MODELE
###
#################################
*/


//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;

//	Classe Book qui permet les interactions avec la base de donnée
//	elle hérite de la classe Modele
//	Elle permet de récupérer les données concernant les livres dans la BDD
class Book extends mvc\Model
{
	//	Déclaration des variables de l'objet
	public $book_id;
	public $title;
	public $author;
	public $author_id;
	public $synopsis;
	public $category;
	public $photo;
	public $coms;


//	Constructeur de la classe,
//	Quand on initialise une nouvelle instance, récupère les data mises en paramètres
//	Et les ajoutes à l'objet
	public function __construct($id = "", $title = "", $author = "", $author_id = "", $synopsis = "", $category = "", $photo, $coms = "")
	{
		$this->book_id = $id;
		$this->title = $title;
		$this->author = $author;
		$this->author_id = $author_id;
		$this->synopsis = $synopsis;
		$this->category = $category;
		$this->photo = $photo;
		$this->coms = $coms;
	}

//	Récupère toutes les données de la table
	public static function getAll ()
	{

		//	Instancie la base de donnée
		$bdd = self::setBdd ();

		//	Prépare la requete
		$sql = "SELECT * FROM iReadBook_book";
		
		//	Lance la requete
		$requete = $bdd->query($sql)->fetchAll();

		//	Lis tous les éléments récupéré par la requete et crée un nvl objet pour chaque
		foreach ($requete as $book) {
			$books[] = new Book(
				$book['id'],
				$book['title'],
				// 	La table donne seulement l'id de l'auteur, ici on appelle le modele Author pour récuperer son nom
				self::getAuthor($book['fk_author_id']),	
				$book['fk_author_id'],
				$book['synopsis'],
				$book['category'],
				$book['img']);
		}

		//	Retourne le tableau d'objet au controlleur
		return $books;
	}

//	Fonction pour récupérer tous les livres en BDD dont le titre contiens la variable $search
	public static function search ($search)
	{
		//	On initialise la base de donnée
		$bdd = self::setBdd ();

		//	On prepare la requete Pour récupéré l'id de l'auteur
		//	Correspondant à la recherche
		$sql = "SELECT id FROM iReadBook_author WHERE name LIKE '%$search%'";

		//	On lance la requette
		$author_id = $bdd->query($sql)->fetch()["id"];

		//	On prepare la requete
		//	On injecte l'id de l'auteur récupéré comme cela
		//	Si la recherche se fait par le nom d'un auteur
		//	On récupère ses livres
		$sql = "SELECT * FROM iReadBook_book WHERE title LIKE '%$search%' OR fk_author_id = '$author_id'";

		//	On lance la requette
		$requete = $bdd->query($sql)->fetchAll();
		$books = [];

		//	Boucle pour chaque élément de la requete, on crée un nouvel objet que l'on stock dans un tableau
		foreach ($requete as $book) {
			$books[] = new Book(
				$book['id'],
				$book['title'],
				self::getAuthor($book['fk_author_id']),
				$book['fk_author_id'],
				$book['synopsis'],
				$book['category'],
				$book['img']);
		}



		return $books;
	}

//	Fonction pour récupérer tous les livres en BDD dont la catégorie correspond à celle donnée en paramètre
	public static function getByTag ($tag)
	{
		//	On initialise la base de donnée
		$bdd = self::setBdd ();

		//	On prepare la requete
		$sql = "SELECT * FROM iReadBook_book WHERE category = '$tag'";
		$requete = $bdd->query($sql)->fetchAll();
		$books = [];

		//	Boucle pour chaque élément de la requete, on crée un nouvel objet que l'on stock dans un tableau
		foreach ($requete as $book) {
			$books[] = new Book(
				$book['id'],
				$book['title'],
				//	On récupere l'auteur
				self::getAuthor($book['fk_author_id']),
				$book['fk_author_id'],
				$book['synopsis'],
				$book['category'],
				$book['img']);
		}

		return $books;
	}

//	Fonction pour récupérer tous les livres en BDD dont l'id correspond à celui donné en paramètre
	public static function getById ($id)
	{
		$bdd = self::setBdd ();
		$sql = "SELECT * FROM iReadBook_book WHERE id = $id";
		//	Une seule valeur est censée etre retournée donc Fetch au lieu de FetchALl
		$book = $bdd->query($sql)->fetch();

		//	Necessite pas de ForEach puisqu'une seule valeur est retournée
		$book = new Book(
				$book['id'],
				$book['title'],
				self::getAuthor($book['fk_author_id']),
				$book['fk_author_id'],
				$book['synopsis'],
				$book['category'],
				$book['img'],
				Coms::getAll($book["id"]));

		return $book;
	}

//	Fonction pour récupérer tous les livres en BDD dont l'auteur correspond à celui donné en paramètre
	public static function getByAuthor ($author_id)
	{	
		$bdd = self::setBdd ();
		$sql = "SELECT * FROM iReadBook_book WHERE fk_author_id = $author_id";
		$res = $bdd->query($sql)->fetchAll();

		foreach ($res as $book) {
			$books[] = new Book(
					$book['id'],
					$book['title'],
					//	On récupere l'auteur
					self::getAuthor($book['fk_author_id']),
					$book['fk_author_id'],
					$book['synopsis'],
					$book['category'],
					$book['img']);
		}
		return $books;
	}

//	Fonction qui créée un nouveau livre dans la base de donnée
	public static function create ($data)
	{
		$bdd = self::setBDD();
            try 
            {
            	//	Prepare la requete, permet d'eviter certaines erreurs
        	    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        	    
//				 On insert les nvlles data dans la bdd

//###################
//##	Vérification de l'existance de l'auteur
//###################
        	    //	Commande sql, on cherche si l'auteur existe
        	    $sql = "SELECT id FROM iReadBook_author WHERE name = '" . $data['author'] . "'";
        	    $res = $bdd->query($sql);
        		
        		//	On annalyse le retour de la commande si == 0 alors l'auteur n'existe pas,
        		//	il faut donc le creer
        	    if ($res->fetchColumn() > 0) {
	//###################
	//##	Création du livre
	//###################
        	    	//	Commande sql pour ajouter un livre
	        	    $sql = "INSERT INTO iReadBook_book (title, fk_author_id, synopsis, category, img)
	        	    VALUES ('".$data['title']."', (".
	                	    "SELECT id FROM iReadBook_author WHERE name = '" . $data['author'] ."'), '".
	                	    $data['synopsis']."', '".
	                	    $data['category']."', '".
	                	    $data['photo']."')";

	               	//	On lance la requete
	                $bdd->exec($sql);
	                return 1;
				}
	//###################
	//##	Création de l'auteur
	//###################
				else {
        	    	//	Commande sql pour ajouter un auteur
					$sql = "INSERT INTO iReadBook_author (name)
						VALUES ('" . $data['author'] . "')";
					$bdd->exec($sql);

					//	Commande sql pour ajouter un livre
					$sql = "INSERT INTO iReadBook_book (title, fk_author_id, synopsis, category, img)
	        	    VALUES ('".$data['title']."', (".
	                	    "SELECT id FROM iReadBook_author WHERE name = '" . $data['author'] ."'), '".
	                	    $data['synopsis']."', '".
	                	    $data['category']."', '".
	                	    $data['photo']."')";

	                $bdd->exec($sql);
	                return 1;
				}

        	}
        	//	Si erreur il y a eu, l'affiche
        	catch(PDOException $e)
        	{
        	    echo $sql . "<br>" . $e->getMessage();
        	}
	}

//	Fonction qui permet de mettre à jours les Data d'un livre
	public static function update ($data, $id)
	{
		//	On initialise la base de donnée
		$bdd = self::setBDD();
        try 
        {       
            //	Prepare la requete, permet d'eviter certaines erreurs 	    
    	    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	    
    	    // On insert les nvlles data dans la bdd
    	    $sql = "SELECT id FROM iReadBook_author WHERE name = '" . $data['author'] . "'";
    	    $res = $bdd->query($sql);
    
    	    if ($res->fetchColumn() > 0) {
        	    $sql = "UPDATE iReadBook_book SET
        	    	title = '".$data['title']."', 
        	    	fk_author_id = (". "SELECT id FROM iReadBook_author WHERE name = '" . $data['author'] ."'), 
        	    	synopsis = '" . $data['synopsis']."', 
        	    	category = '" . $data['category']."', 
        	    	img = '" . $data['photo']."'
        	    	WHERE id = $id";

                $bdd->exec($sql);
                return 1;
			}
			else {
				$sql = "INSERT INTO iReadBook_author (name)
					VALUES ('" . $data['author'] . "')";
				$bdd->exec($sql);

				$sql = "INSERT INTO iReadBook_book (title, fk_author_id, synopsis, category, img)
        	    VALUES ('".$data['title']."', (".
                	    "SELECT id FROM iReadBook_author WHERE name = '" . $data['author'] ."'), '".
                	    $data['synopsis']."', '".
                	    $data['category']."', '".
                	    $data['photo']."')";

                $bdd->exec($sql);
                return 1;
			}

    	}
    	catch(PDOException $e)
    	{
    	    echo $sql . "<br>" . $e->getMessage();
    	}
	}

//	Fonction qui permet de supprimer un livre de la Bdd
	public static function delete ($id)
	{
		$bdd = self::setBdd ();
		$sql = "DELETE FROM iReadBook_book WHERE id = $id";
		$bdd->exec($sql);
	}

//	Fonction qui permet de recuperer l'auteur d'un livre
	private static function getAuthor($author_id)
	{
		return Author::getById($author_id)->name;
	}
}

?>