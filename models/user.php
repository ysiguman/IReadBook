<?php
/*
#################################
###
###		USER MODELE
###
#################################
*/


//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;

//	Classe User qui permet les interactions avec la base de donnée
//	elle hérite de la classe Modele
class User extends mvc\Model
{
	//	Différentes variables de l'objet
	public static $user_id;
	public static $user_name;
	public static $pass;

//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle associe les paramètes aux variables de la classe
	public function __construct($name = "", $pass = "")
	{
		self::$user_name = $name;
		self::$pass = $pass;
	}

//	Fonction de création de nouveau utilisateurs
	public static function create ()
	{
		//	On initialise la base de donnée
		$bdd = self::setBdd();

		$bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	 
	    $sql = "INSERT INTO iReadBook_user (name, password)
	            VALUES ('" . self::$user_name . "', '" . self::$pass . "')";
	    $bdd->exec($sql);
	}

//	Récupère dans la base de donnée le nom de l'user via son id
	public static function getName ($id)
	{
		$bdd = self::setBdd ();
		$sql = "SELECT * FROM iReadBook_user WHERE id = $id";
		$author = $bdd->query($sql, PDO::FETCH_OBJ)->fetch();

		return $author->name;
	}

// 	Fonction qui permet de tester si le mot de passe et le nom est ok
//	et ainsi de demarrer une ouvelle fonction
	public static function connexion()
	{
		$bdd = self::setBdd ();
		$req = $bdd->prepare('SELECT * FROM iReadBook_user WHERE name = :name AND password = :pass');
    	$req->execute(array(
    	    'name' => self::$user_name,
    	    'pass' => self::$pass)); 

    	$res = $req->fetch();  // Recupere le resultat
		if (!$res) // Si aucun resultat n'est retourné, on considère que l'utilisateur n'existe pas
    	{
    	    return 0;
    	}
    	else // Sinon on ouvere la session
    	{
    		session_start(); // On demare la session, permet d'acceder aux variables Seesion
    	    self::$user_id = $res["id"];
    	    self::$user_name = $res["name"];
    	    self::$pass = $res["pass"];

    	    $_SESSION['id'] = self::$user_id;
    	    $_SESSION['name'] = self::$user_name;

    	    return 1;
    	}
	}
}
?>