<?php
/*
#################################
###
###		TAG MODELE
###
#################################
*/


//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;


//	Classe Tag qui permet les interactions avec la base de donnée
//	elle hérite de la classe Modele
//	Elle permet de récupérer les données concernant les tags dans la BDD
class Tag extends mvc\Model
{
	//	Variables de l'objet
	public $name;

//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle associe les paramètes aux variables de la classe
	public function __construct($name)
	{
		$this->name = $name;
	}

//	Fonction qui permet de récuperer tous les tags présent en base de donnée
	public static function getAll ()
	{
		//	On initialise la base de donnée
		$bdd = self::setBdd ();

		//	On prepare la requete
		$sql = "SELECT category FROM iReadBook_book GROUP BY category";
		
		//	On lance la requette
		$requete = $bdd->query($sql)->fetchAll();
		$tags = [];

		foreach ($requete as $tag) 
		{
			if ($tag['category'] !== "") 
			{
				$tags[] = new Tag(
					$tag['category']);
			}
		}

		return $tags;
	}

	public static function getBooks ($name)
	{
		global $router;
		echo  $router->getRoute("Tags#getByName", $name);
	}
}

?>