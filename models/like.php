<?php

/*
#################################
###
###		LIKE MODELE
###
#################################
*/

//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;

//	Classe Like qui permet les interactions avec la base de donnée
//	elle hérite de la classe Modele
//	Elle permet de récupérer les données concernant les likes dans la BDD
class Like extends mvc\Model
{
	//	Différentes variables de l'objet
	public $nbLikes;
	public $liked;

//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle associe les paramètes aux variables de la classes
	public function __construct($nbLikes, $isLike)
	{
		//	Différentes variables de l'objet
		$this->nbLikes = $nbLikes;
		$this->liked = $isLike;
	}

//	Fonction qui permet de récuperer tous les likes présent en base de donnée
	public static function getAll ($book_id)
	{
		//	On initialise la base de donnée
		$bdd = self::setBdd ();

		//	On prepare la requete
		$sql = "SELECT COUNT(*) AS nb FROM iReadBook_like WHERE fk_book_id = $book_id";

		//	On lance la requette
		$requete = $bdd->query($sql)->fetch();

		//	On appelle une fonction qui permet de voir si le livre est liké par l'utilisateur
		$isLike = self::isLiked($book_id);

		//	On creer un nouvel objet que l'on renvoie
		$like = new Like($requete["nb"], $isLike); 
		
		return $like;
	}

// 	Fonction permettant de supprimer un like
	public static function isLiked ($book_id)
	{
		//	On vérifie qu'un utilisateur est connecté
		if (isset($_SESSION["name"])) 
		{
			//	On récupère l'id de l'utilisateur
			$user_id = $_SESSION['id'];
		
			//	On prépare la base de donnée
			$bdd = self::setBdd ();

			//	On prépare la requete
			$sql = "SELECT COUNT(*) AS nb FROM iReadBook_like WHERE fk_book_id = $book_id AND fk_user_id = $user_id";
			
			//	On lance la requete et renvoie le résultat
			$requete = $bdd->query($sql)->fetch();
			return $requete["nb"];
		}
		return 0;
	}

//	Fonction permettant d'ajouter un like
	public static function addLike ($book_id, $user_id)
	{
		//	On initialise la base de donnée
		$bdd = self::setBDD();
        try 
        {   
        	//	On prepare la requete, a permis d'eviter certaines erreures     	    
    	    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	    
    	    //	On prepare le SQL
    	    $sql = "INSERT INTO iReadBook_like (fk_book_id, fk_user_id)
    	    VALUES ('" . $book_id . "', '".
            	    $user_id."')";

	        //	On lance la requete
            $bdd->exec($sql);

    	}
    	catch(PDOException $e)
    	{
    		//	Affiche une erreur si la requete n'as pas fonctionnée
    	    echo $sql . "<br>" . $e->getMessage();
    	    return 0;
    	}
	}

//	Fonction permettant d'enlever le like ajouté
	public static function disLike ($book_id, $user_id)
	{
		//	On initialise la base de donnée
		$bdd = self::setBDD();
        try 
        {        	    
    	    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    	    $sql = "DELETE FROM iReadBook_like WHERE fk_book_id= $book_id AND fk_user_id = $user_id";
	       
	       	//	On lance la requete
            $bdd->exec($sql);

            return 1;

    	}
    	catch(PDOException $e)
    	{
    	    echo $sql . "<br>" . $e->getMessage();
    	    return 0;
    	}
	}

//	Fonction qui récupère tous les likes de l'utilisateur $user_id
	public static function userLikes($user_id)
	{
		//	On initialise la base de donnée
		$bdd = self::setBdd ();
		$sql = "SELECT * FROM iReadBook_like WHERE fk_user_id = $user_id";
		$requete = $bdd->query($sql)->fetchAll();

		//	On boucle le résultat renvoyé et on créer pour chaque élément un objet Book
		$books = [];
		foreach ($requete as $book) {
		 	$books[] = Book::getById($book["fk_book_id"]);
		 } 
		
		return $books;
	}
}

?>