test
# Projet Web A1
Website to list and comments books.
__Groupe Justin DUPRES & Victor DUTEURTRE__

## Read-Me - Initialisation
### Étapes
1. Mettre le dossier du projet sur le serveur
2. Rentrer la config du serveur dans le fichier config.json à la racine du projet, `baseDir` permet de definir le chemin vers le dossier contenant le projet (exemple plus bas)
3. Modifier le fichier `.htaccess`: si comme precedement le projet n'est pas à la racine du serveur, il faut rentrer le chemin du projet avant le index.php sur la ligne 5 (exemple plus bas) 

### Exemples
* Le projet est à la racine du serveur:
    - Laisser le fichier `.htaccess` tels quel
    - Remplir la configuration de la base de donnée dans le fichier `config.json`
* Le serveur suis l'architecture suivante:
    * www
        * index.php
        * Dossier1
        * Dossier2
        * IReadBooks // Nom de fichier exemple, notre projet se situ en dessous
            * fichiers / dossiers du projet
    - Dans ce cas il faut mettre ./IReadBooks/index.php dans htaccess
    - et /IReadBooks dans config.json pour baseDir

## TO-DO
- [x] Likes
- [x] Listes
- [x] Tags
- [x] Commentaires
- [x] Syncho avec Wikipédia
- [ ] AutoComplétion champ recherche
- [x] AutoComplétion Auteurs / Tags nouveau livre