<?php
/*
#################################
###
###     USERS CONTROLLER
###
#################################
*/


//  On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//  pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;

//  Classe Users qui permets les interactions entre certaires vues, le modèle Users et
//  l'utilisateur, elle hérite de la classe Controller
//  Elle permet de récupérer les données concernant les users, les structures et les 
//  envoies à la vue
class Users extends mvc\Controller
{
    public function __construct() {
        include_once(DIR_MODEL."user.php");
    }

//  Fonction qui permet à un utilisateur de se connecter 
    public static function connexion ()
    {
        global $router;
        //  On récupère les ids que l'utilisateur à rentré
        $name = $_POST["name"];
        $pass = sha1($_POST["pass"]);

        $user = new User($name, $pass);

        $conn = $user::connexion();
        if ($conn === 1)
        {
            header('Location: ' . $_SERVER['HTTP_REFERER']); // On prépare la redirection    
        } else
        {
            header('Location: ' . $router->getRoute("Users#error_connect")); // On prépare la redirection    
        }
    }

//  Création d'un utilisateur avec le nom ainsi que le mot de passe
    public static function createUser ()
    {
        $name = $_POST["name"];
        $pass = sha1($_POST["pass"]);
        if($name === "" || $pass ==="")
        {
            header('Location: ' . $router->getRoute("Users#error_connect")); // On prépare la redirection  
        }   else
        {
            //  Creer un nouvel objet USer
            $user = new User($name, $pass);

            //  Lance les fonctions de création et de connexion d'un utilisateur
            $user::create();
            $user::connexion();

            header('Location: ' . $_SERVER['HTTP_REFERER']);
        }
    }

//  Fonction pour se déconecter 
    public static function disconnect ()
    {
        session_start(); // On ouvre la session
        
        // On detruit la session en cours
        session_destroy();
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }

    public static function error_connect ()
    {
        session_start();
        if (!isset($_SESSION["id"])) {
            // On initialise une nouvelle vue avec les livres récupèrées en paramètres
            $view = new mvc\View("error_connect", "");
            $view->render();
        } else
        {
            header('Location: /');
        }
    }
}
?>