<?php
/*
#################################
###
###		AUTHORS CONTROLLER
###
#################################
*/



//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;

//	Classe Authors qui permets les interactions entre certaires vues, le modèle Author et
//	l'utilisateur, elle hérite de la classe Controller
//	Elle permet de récupérer les données concernant les auteurs, les structures et les 
//	envoies à la vue
class Authors extends mvc\Controller
{

//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle charge les différents fichier qui pourraient etre necessaire lors
//	du traitement des datas
	public function __construct ()
	{
		include_once(DIR_MODEL."book.php");
		include_once(DIR_MODEL."author.php");
	}

//	Fonction pour récuperer Tous les authors en BDD
	public static function getAll ()
	{
		//	On récupère chaques auteurs en appelant le modèle
		$authors = Author::getAll();
		
		// On initialise une nouvelle vue avec les livres récupèrées en paramètres
		$view = new mvc\View("listAuthors", $authors);
		$view->page = "authors";
		$view->render(); //création de la vue 
	}

//	Fonction qui récupère toutes les data de l'auteur via son ID
	public static function getBooks ($id)
	{
		// On appel le modèle en passant l'id du livre et l'auteur rechercher
		$books = Book::getByAuthor($id); //Livre par son auteur
		$author = Author::getById($id);

		// Envois des données à la vue via un object qui fusione les deux élement récupéré en un unique tableau
		$view = new mvc\View("booksLists", (object) array_merge((array) $author, [ "books" => $books]));
		$view->page = "authors";
		$view->render(); //création de le vue
	}

//	Fonction qui récupère toutes les data de l'auteur via son ID
	public static function getById ($id)
	{
		$book = Book::getById($id); //Livre par son Id
		$author = Author::getById($book->author_id);
		print_r($book); //affiche le tableau book
	}

//	Fonction qui permet de creer un nouveau livre via l'id de l'auteur
	public static function create ()
	{
		$title	=	$_GET["title"];
		$author_id	=	$_GET["author_id"];
		$category_id	=	$_GET["category_id"];
		$synopsis	=	$_GET["synopsis"];
		
		//	On récupère les différents champs envoyé par la requète
		$data = array(
	        "title" => $title,
	        "author_id" => $author_id,
	        "synopsis" => $synopsis,
	        "category_id" => $category_id
	    );

	    print_r($data); //affiche le tableau data

		return Book::create($data); //lance la création de la fonction auteur
	}
//	Fonction qui permet de supprimer un livre de la Bdd
	public static function delete ($id)
	{
		global $router;
		header("Location: " . $router->getRoute("Authors#getAll"));
		return Author::delete($id);
	}
}

?>