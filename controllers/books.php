<?php
/*
#################################
###
###		BOOKS CONTROLLER
###
#################################
*/


//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;

//	Classe Books qui permets les interactions entre certaires vues, le modèle Book et
//	l'utilisateur, elle hérite de la classe Controller
//	Elle permet de récupérer les données concernant les livres, les structures et les 
//	envoies à la vu  
class Books extends mvc\Controller
{

//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle charge les différents fichier qui pourraient etre necessaire lors
//	du traitement des datas
	public function __construct ()
	{
		include_once(DIR_MODEL."book.php");
		include_once(DIR_MODEL."author.php");
		include_once(DIR_MODEL."comment.php");
	}

//	Fonction pour recuperer tous les livres en BDD
	public static function getAll ()
	{
		//	On récupère chaque livres en appelant le modèle
		$books = Book::getAll();

		// On initialise une nouvelle vue avec les livres récupèrées en paramètres
		$view = new mvc\View("list", $books);
		$view->page = "books";
		$view->render();	//	création de la vue
	}

//	Fonction pour récupérer tous les livres en BDD dont le titre contiens la variable envoyé en POST 
	public static function search ()
	{
		//	On récupère le champ à rechercher et on appel le modèle pour récuperer les livres
		$search = $_GET["search"];
		$books = Book::search($search);

		// On gènère la vue avec les livres récupèrés
		$view = new mvc\View("list", $books);
		$view->page = "books";
		$view->render(); // création de la vue
	}

//	Fonction qui récupère toutes les data d'un livre via son ID
	public static function getById ($id)
	{
		// On appel le modèle en passant l'id du livre rechercher
		$book = Book::getById($id);

		$view = new mvc\View("book", $book);
		$view->render();
	}

//	Fonction qui permet de creer un nouveau livre
	public static function create ()
	{

		//	On récupère les différents champs envoyé par la requète
		$title	=	addslashes($_POST["title"]);
		$author	=	addslashes($_POST["author"]);
		$category	=	addslashes($_POST["category"]);
		$synopsis	=	addslashes($_POST["synopsis"]);

		// On determine si une image à étée uploadée
		if(!is_uploaded_file($_FILES["photo"]["tmp_name"])) // On test si un fichier à été uploader
		{ 
		    $file_name = "test.jpg"; // si non on donne un nom apr defaut
		}
		else
		{
		    // On definit les variables pour le fichier
		    $target_dir = DIR_PHOTOS; // emplacement de destination
		    $target_file = $target_dir . basename($_FILES["photo"]["name"]); // nom du ficher
		    $file_name = basename($_FILES["photo"]["name"]); // On récupere le nom
		    $uploadOk = 1;	//	On definit la variable 
		    
		    if(isset($_POST["submit"])) {
		        $check = getimagesize($_FILES["photo"]["tmp_name"]); // On verifie que c'est bien une image
		        if($check !== false) {
		            $uploadOk = 1;
		        } else {
		            $uploadOk = 0;
		        }
		    }
		    if (file_exists($target_file)) {  // On verifie que l'image n'existe pas (on annule si l'image existe deja)
		        $uploadOk = 0;
		    }
		    if ($_FILES["size"] > 500000) { // On verifie que l'image est < 5Mo
		        $uploadOk = 0;
		    }
		    // On effectue le deplacement vers le dossier de destination
		    if (!move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file) && $uploadOk) { 
		        echo "Erreur d'upload.";
		    }
		}

		// On stocke toutes les infos à Enregistrer
		$data = array(
	        "title" => 	$title,
	        "author" => $author,
	        "synopsis" => 	$synopsis,
	        "category" => 	$category,
	        "photo" => 	$file_name
	    );

		// Lance la redirection puis enregistre les données
		header("Location: /");
		return Book::create($data);
	}

//	Fonction qui permet de mettre à jours les Data d'un livre
	public static function update ($id)
	{
		$title	=	addslashes($_POST["title"]);
		$author	=	addslashes($_POST["author"]);
		$category	=	addslashes($_POST["category"]);
		$synopsis	=	addslashes($_POST["synopsis"]);

		if(!is_uploaded_file($_FILES["photo"]["tmp_name"])) // On test si un fichier à été uploader
		{
		    $file_name = basename($_FILES["photo"]["name"]); // si non on donne un nom apr defaut
		}
		else
		{
		    // On definit les variables pour le fichier
		    $target_dir = DIR_PHOTOS; // emplacement de destination
		    $target_file = $target_dir . basename($_FILES["photo"]["name"]); // nom du ficher
		    $file_name = basename($_FILES["photo"]["name"]); // On récupere le nom
		    $uploadOk = 1;
		    
		    if(isset($_POST["submit"])) {
		        $check = getimagesize($_FILES["photo"]["tmp_name"]); // On verifie que c'est bien une image
		        if($check !== false) {
		            $uploadOk = 1;
		        } else {
		            $uploadOk = 0;
		        }
		    }
		    if (file_exists($target_file)) {  // On verifie que l'image n'existe pas (on annule si l'image existe deja)
		        $uploadOk = 0;
		    }
		    if ($_FILES["photo"]["size"] > 500000) { // On verifie que l'image est < 5Mo
		        $uploadOk = 0;
		    }
		    
		    if (!move_uploaded_file($_FILES["photo"]["tmp_name"], $target_file)) { // On effectue le deplacement vers le dossier de destination
		        echo "Erreur d'upload.";
		    }
		}

		$data = array(
	        "title" => $title,
	        "author" => $author,
	        "synopsis" => $synopsis,
	        "category" => $category,
	        "photo" => $file_name
	    );

		header("Location: /");
		return Book::update($data, $id);

	}

//	Fonction qui permet de supprimer un livre de la Bdd
	public static function delete ($id)
	{
		header("Location: /");
		return Book::delete($id);
	}

//	Focntion qui permet de générer la page nouveau livre
	public static function newBook ()
	{
		session_start();
		if(isset($_SESSION["id"]))
		{
			global $router;
			$data = array(
		        "title" => "",
		        "author" => "",
		        "synopsis" => "",
		        "category" => "",
		        "post_url" => $router->getRoute("Books#create")
		    );

			$view = new mvc\View("new", $data);
			$view->page = "new";
			$view->render();
		}	else
		{
			header("Location: /");
		}
	}

//	Fonction qui permet de génerer la page Mise à jour de Livre
	public static function updateBook ($id)
	{
		session_start();
		if(isset($_SESSION["id"]))
		{
			$book = Book::getById($id);

			global $router;
			$data = array(
		        "title" => $book->title,
		        "author" => $book->author,
		        "synopsis" => $book->synopsis,
		        "category" => $book->category,
		        "post_url" => $router->getRoute("Books#update", $id)
		    );

			$view = new mvc\View("new", $data);
			$view->page = "update";
			$view->render();
		}	else
		{
			header("Location: /");
		}
	}

}

?>