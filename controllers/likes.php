<?php
/*
#################################
###
###		Likes CONTROLLER
###
#################################
*/

//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;

//	Classe Likes qui permets les interactions entre certaires vues, le modèle Like et
//	l'utilisateur, elle hérite de la classe Controller
//	Elle permet de récupérer les données concernant les likes
class Likes extends mvc\Controller
{
//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle charge les différents fichier qui pourraient etre necessaire lors
//	du traitement des datas
	public function __construct ()
	{
		include_once(DIR_MODEL."like.php");
	}

//	Fonction permettant d'ajouter un like à un livre dont on a récupérer l'id en paramètre
	public static function addLike ($book_id)
	{
		session_start();	//	<session_start()> : permet de démarer ou récupérer une session existante

		//	On test si un utilisateur est effectivement connecté, si oui, on peut ajouter un like
		if(isset($_SESSION['id']))
		{
			return Like::addLike($book_id, $_SESSION['id']); //	Enregistre
		}
	}

//	Fonction permettant de supprimer un like à un livre dont on a récupérer l'id en paramètre
	public static function dislike ($book_id)
	{
		session_start();	//	<session_start()> : permet de démarer ou récupérer une session existante

		//	On test si un utilisateur est effectivement connecté, si oui, on peut lancer la suppression
		if(isset($_SESSION['id']))
		{
			return Like::dislike($book_id, $_SESSION['id']); //	Enregistre
		}
	}

//	Fonction qui récupère tous les livres qui on étés likés par l'utilisateur courant
	public static function myLikes ()
	{
		session_start();	//	<session_start()> : permet de démarer ou récupérer une session existante

		if(isset($_SESSION["id"]))
		{
			//	On récupère les likes de l'utilisateur
			$books = Like::userLikes($_SESSION['id']);

			// On initialise une nouvelle vue avec les likes récupèrées en paramètres
			$view = new mvc\View("booksLists", (object) array_merge((array) [ "books" => $books, "nbBooks" => count($books)], [ "name" => "Likes " . $_SESSION["name"]]));
			$view->user = "my_likes";
			$view->render(); //création de la vue
		}	else
		{
			header("Location: /");
		}
	}
}

?>