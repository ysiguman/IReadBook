<?php
/*
#################################
###
###		COMS CONTROLLER
###
#################################
*/

//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;
//	Classe Coms qui permets les interactions entre certaires vues, le modèle Com et
//	l'utilisateur, elle hérite de la classe Controller
//	Elle permet de récupérer les données concernant les commentaires, les structures et les 
//	envoies à la vue
class Coms extends mvc\Controller
{

//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle charge les différents fichier qui pourraient etre necessaire lors
//	du traitement des datas
	public function __construct ()
	{
		include_once(DIR_MODEL."comment.php");
		include_once(DIR_MODEL."user.php");
	}

// Fonction qui récupère tous les commentaires liés à un livre par son id
	public static function getAll ($book_id)
	{
		$coms = Com::getAll ($book_id);
		return $coms;
	}

//	Fonction qui permet de creer un nouveau commentaire
	public 	static function create ()
	{
		global $router;

		//	On récupère les différents champs envoyé par la requète
		// 	<htmlentities> : permer de "convertir" la string affin d'eviter certaines erreur
		//	lors de la bise en base de donnée comme les guillemé qui saute
		//	<date> : permet de récuperer la date du jour
		$data = array(
			"com" => htmlentities($_POST["com"], ENT_QUOTES),
			"user_id" => $_POST["user_id"],
			"book_id" => $_POST["book_id"],
			"date" => date("Y-m-d H:i:s") );
		Com::create($data);	//	On appel la focntion create dans le modele

		// Lance la redirection puis enregistre les données
		header("Location: " . $router->getRoute("Books#getById", $_POST["book_id"]));
	}
}

?>