<?php
/*
#################################
###
###		TAGS CONTROLLER
###
#################################
*/


//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;

//	Classe Tags qui permets les interactions entre certaires vues, le modèle Tag et
//	l'utilisateur, elle hérite de la classe Controller
//	Elle permet de récupérer les données concernant les tags, les structures et les 
//	envoies à la vu
class Tags extends mvc\Controller
{
//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle charge les différents fichier qui pourraient etre necessaire lors
//	du traitement des datas
	public function __construct ()
	{
		include_once(DIR_MODEL."tag.php");
		include_once(DIR_MODEL."book.php");
	}

//	Fonction pour recuperer tous les tags en BDD
	public static function getAll ()
	{
		$tags = Tag::getAll ();

		//	Retourne les tags récupérés
		return $tags;
	}

// 	Récupère tous les livres avec le meme tag
	public static function getBooks ($name)
	{
		$books = Book::getByTag ($name);

		$view = new mvc\View("booksLists", (object) array_merge((array) [ "books" =>$books, "nbBooks" => count($books)], [ "name" => $name]));
		$view->render(); //	Enregistre la vue
	}
}

?>