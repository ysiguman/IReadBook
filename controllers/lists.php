<?php
/*
#################################
###
###		Lists CONTROLLER
###
#################################
*/


//	On utilise le module MVC (définit dans le fichier "mvc.php" du dossier mvc)
//	pour appeler les classes du ficher on aura à rajouter "mvc\" devant leur noms
use MVC as mvc;
//	Classe Lists qui permets les interactions entre certaires vues, le modèle List et
//	l'utilisateur, elle hérite de la classe Controller
//	Elle permet de récupérer les données concernant les livres, les structures et les 
//	envoies à la vue
class Lists extends mvc\Controller
{
//	Fonction constructor appellée lors de l'initialisation de la classe,
//	Elle charge les différents fichier qui pourraient etre necessaire lors
//	du traitement des datas
	public function __construct ()
	{
		include_once(DIR_MODEL."list.php");
		include_once(DIR_MODEL."book.php");
	}

//	Fonction pour recuperer tous les lists en BDD
	public static function getAll ()
	{
		//	On récupère chaques listes en appelant le modèle
		$lists = ListBook::getAll ();
	
		//	On initialise une nouvelle vue avec les listes récupèrées en paramètres
		$view = new mvc\View("listLists", $lists);
		$view->page = "lists";
		$view->render(); 	//	Création de la vue
	}

//	Récupère les listes et leur état (si le livre est listé dans cette liste) pour un 
//	certain livre donné en paramètre
	public static function getForBook ($id)
	{
		//	On prépare le type de document de retour: json puisque l'on revoit du json
		header('Content-Type: application/json');

		//	On récupère les listes
		$books = ListBook::getForBook($id);
		echo json_encode($books);
	}

//	On récupère tous les livres présents sur une liste
	public static function getBooks ($id)
	{
		$books = ListBook::getById ($id);

		//	On initialise une nouvelle vue avec les livres de la liste récupèrées en paramètres
		$view = new mvc\View("booksLists", $books);
		$view->page = "lists";
		$view->render(); //création de la vue
	}

//	Fonction qui permet de supprimer une liste de la Bdd
	public static function delete ($id)
	{
		global $router;
		//	<header> : permet de gerer la redirection qui fera suite à la suppression
		//	<$router->getRoute(...)> : permet de récupérer l'url qui renvoie vers le controller ...
		header("Location: " . $router->getRoute("Lists#getAll"));
		return ListBook::delete($id);
	}

//	Récupère toutes les listes de l'utilisateur donné en paramètre
	public static function listBy ($id)
	{
		$books = ListBook::listBy ($id);

		$view = new mvc\View("listLists", $books);
		$view->page = "lists";
		$view->user = "lists";
		$view->render();
	}

//	Fonction permettant d'ajouter un livre dans la liste sont id est donné en paramètre
	public static function addBook ($list_id)
	{
		$book_id = $_POST["bookID"];
		echo ListBook::addBook($list_id, $book_id);
	}

// Fonction permmettant d'enlever un livre dans la liste sont id est donné en paramètre
	public static function remove ($list_id)
	{
		$book_id = $_POST["bookID"];
		echo ListBook::remove($list_id, $book_id);
	}

// Fonction permettant la création d'une liste
	public static function create()
	{
		session_start();
		header("Location: /");
		$list_name = $_POST["name"];
		$list_user_id = $_SESSION["id"];

		ListBook::create($list_name, $list_user_id);
	}
}

?>